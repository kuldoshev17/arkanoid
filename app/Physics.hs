module Physics where
import Constants
import Graphics.Gloss
import Data.List
import Data.Maybe
import System.Random

-- | Possible types of collisions
-- top, bottom, right, left, corner, side, none
data ColType = T | B | R | L | C | S | N
  deriving (Eq)


-- | Change velocity of the ball and destroy hit block.
blockCollision :: GameStatus -- Initial game status.
               -> GameStatus -- Game status after collision.
blockCollision game = result
  where
    blcks = blocks game

    hasCollision = not (null collision)
    result
      | hasCollision = game { ballVel = newAcc, blocks = newBlocks, score = newScore }
      | otherwise = game

    oldScore = score game
    newScore = oldScore + 10

    newBlocks = filter (\a -> blockType a /= 0) newBlocks'
    newBlocks' = map (\(x,y)-> if x /= N then y { blockType = blockType y - 1} else y ) collisionType
    collision = filter (\(x, y) -> x /= N) collisionType
    collisionType = map (\x ->
           (circleRectCollision (ballLoc game) ballSize (blockPos x) blockSize, x)
       ) blcks

    ct =
      case listToMaybe collision of
        Just (a,b) -> a
        Nothing    -> N

    newAcc
      | ct == R || ct == L = (-xv,yv)
      | ct == T || ct == B = (xv,-yv)
      | ct == C            = (-xv,-yv)
      | otherwise          = (xv,yv)

    (xv,yv) = ballVel game

-- | Slightly optimize. Do not check if it is impossible
collisionsWithPaddle :: (Float, Float)
  -> Float -> (Float, Float) -> (Float, Float) -> ColType
collisionsWithPaddle (cx,cy) cSize (rx,ry) (rw,rh) = result
  where
    result
      | pl < cy - cSize = N
      | otherwise = circleRectCollision (cx,cy) cSize (rx,ry) (rw,rh)
    pl = pLvl myPaddle + phh
    phh = pHeight myPaddle / 2

-- | Identifying collision type based on hit angle
circleRectCollision :: (Float,Float) -> Float -> (Float,Float) -> (Float,Float) -> ColType
circleRectCollision (cx,cy) cSize (rx,ry) (rw,rh) = result
  where
      result
        | not isCollisionPossible = N
        | sideCollision /= N = sideCollision
        | cornerCollision = C
        | otherwise = N
      -- | Some optimization, check only possible blocks
      isCollisionPossible = hypotenuse + cSize >= distance (cx,cy) (rx,ry)
      hypotenuse = sqrt ((rw/2)^2 + (rh/2)^2)
      corns = corners (rx,ry) (rw,rh)
      possibleCorns = filter (\corn -> distance corn (cx,cy) <= cSize ) corns
      cornerCollision = not (null possibleCorns)
      (t,r,b,l) = sides (rx,ry) (rw,rh)

      sideCollision
        -- ^ top
        | ry < cy && t >= cy - cSize && cx > minX && cx < maxX = T
        -- ^ right
        | rx < cx && r >= cx - cSize && cy > minY && cy < maxY = R
        -- ^ bottom
        | ry > cy && b <= cy + cSize && cx > minX && cx < maxX = B
        -- ^ left
        | rx > cx && l <= cx + cSize && cy > minY && cy < maxY = L
        | otherwise = N
      maxY = t + offs
      minY = b - offs
      maxX = r + offs
      minX = l - offs
      -- | since it is possible that a ball can hit the corner,
      -- we should check slightly more
      offs = cSize * 0.52 -- ^ sin (pi/6)

-- | Get the corner points of the rectangle
-- .---------------.
-- |               |
-- |               |
-- .---------------.
corners :: (Fractional b, Fractional a) => (a, b) -> (a, b) -> [(a, b)]
corners (cx,cy) (w, h) = tl:tr:br:[bl]
  where
    tl = (cx - hw, cy + hh)
    tr = (cx + hw, cy + hh)
    br = (cx + hw, cy - hh)
    bl = (cx - hw, cy - hh)
    hw =  w/2
    hh =  h/2

-- | Get edge-central points for rectangle
-- y coord for top and bottom, x-coord for left and right
-- ---------.--------
-- |                |
-- .                .
-- |                |
-- ---------.--------
sides :: (Fractional c, Fractional d) => (d, c) -> (d, c) -> (c, d, c, d)
sides (cx,cy) (w, h) = (t,r,b,l)
  where
    t = cy + hh
    r = cx + hw
    b = cy - hh
    l = cx - hw
    -- | half width and half height
    hw = w / 2
    hh = h / 2


-- | Get a distance between two points
distance :: Floating a => (a, a) -> (a, a) -> a
distance (x1,y1) (x2,y2) = sqrt ((x2 - x1)^2 + (y2 - y1)^2)


-- | Detect collision with a paddle. Upon collision,
-- change the velocity of the ball to bounce it off.
paddleBounce :: GameStatus -- ^ Initial game status.
             -> GameStatus -- ^ Game status after the bounce.
paddleBounce game = newGame
  where
    newGame
      | collisionT == N = game
      | otherwise       = game {
        ballVel = newBallVel,
        ballLoc = newBallPos,
        paddleBounceCounter = newCounter
      }

    (vx, vy) = ballVel game -- ^ The old velocity of the ball
    (bx, by) = ballLoc game -- ^ The old location of the ball
    px = playerLoc game     -- ^ Player location on X axis
    pv = playerVel game     -- ^ Player velocity
    -- | Get collision type
    collisionT = paddleCollision game
    -- | Add some extra vx velocity for the paddle.
    -- The absolute value of velocity increases towards edges
    bonusVx = signum (bx - px ) * (bx - px )^2 * 0.005
    -- | Get new velocity as old + before mentioned bonus + tangential acceleration
    newVx = vx + bonusVx + vx * (vx / sqrt ((vx^2) + (vy^2))) + pv * 0.1

    -- | Limit possible velocity up to 100, preserve the directions (sign)
    newVxLimited
      | abs newVx > 100 = signum newVx * 100
      | otherwise       = newVx

    -- | The new value of the ball velocity
    -- If it is side collision add some extra velocity, 
    -- because ball can stick inside the paddle otherwise
    newBallVel
      | collisionT == T                    = (newVxLimited, -vy)
      | collisionT == R || collisionT == L = (pv + 15     , -vy)
      | otherwise                          = (vx          ,  vy)

    -- | Get new ball position of the ball
    -- Take into account case when paddle can have bigger velocity
    -- and can "swallow up" the ball. 
    -- So just move ball away the paddle in this case
    newBallPos
      | collisionT == L = (px - phw - ballSize , by)
      | collisionT == R = (px + phw + ballSize , by)
      | collisionT == T = (bx                  , pl + phh + ballSize )
      | otherwise       = (bx                  , by)
    
    -- | Update paddle bounce counter in case of collision
    oldCounter = paddleBounceCounter game
    newCounter = oldCounter + 1

    -- | Paddle level (height, y coord) where paddle is slides
    pl  = pLvl myPaddle
    -- | Paddle sizes (half of it)
    phw = pWidth myPaddle / 2
    phh = pHeight myPaddle / 2


-- | Given position of the ball, return whether
-- a collision with paddle occurred.
paddleCollision :: GameStatus -> ColType    
paddleCollision game =
  collisionsWithPaddle bl ballSize (paddlePosition, playerLvl) pSize
    where
      bl = ballLoc game
      playerLvl = pLvl myPaddle
      paddlePosition = playerLoc game
      pSize = (pWidth myPaddle, pHeight myPaddle)

-- | Detect collision with  wall. Upon collision,
-- update velocity of the ball to bounce it off.
wallBounce :: GameStatus -- ^ Old game status
           -> GameStatus -- ^ Game status after the bounce
wallBounce game = game { ballVel = (vx', vy') }
  where
    (vx, vy) = ballVel game -- ^ The old velocity of the ball
    (x, y)   = ballLoc game -- ^ Position of the ball
    -- New velocity
    vy' = if wallCollision (x, y) == T then -vy else vy
    vx' = if wallCollision (x, y) == S then -vx else vx

-- | Given position of the ball, return whether
-- a collision with wall occurred.
-- Takes Ball position, return collision type
wallCollision :: Position -> ColType 
wallCollision (x, y) = result
  where
    result
      | topCollision = T
      | leftCollision || rightCollision = S
      | otherwise = N
    topCollision = y + ballSize >= hh - borderW 
    leftCollision = x - ballSize <= -hw + borderW 
    rightCollision = x + ballSize >= hw - borderW 
    -- | Size of the game field (half of it)
    hw = fromIntegral width / 2
    hh = fromIntegral height / 2


-- | Update the ball position
moveBall :: Float      -- ^ Number of seconds since last update
         -> GameStatus -- ^ Old game state
         -> GameStatus -- ^ New game state
moveBall seconds game = game { ballLoc = (x', y') }
  where
    -- | Old location and velocity of the ball
    (x, y) = ballLoc game
    (vx, vy) = ballVel game
    -- | New location of the ball
    x' = x + vx * seconds
    y' = y + vy * seconds


-- | Return paddle collision type with walls
-- x coordinate of the player
paddleWallCollision :: Float -> ColType  
paddleWallCollision x = result
  where
    result 
      | x - hpw <= -hw + borderW = L
      | x + hpw >=  hw - borderW = R
      | otherwise                = N

    hpw = pWidth myPaddle / 2
    hw  = fromIntegral width / 2

-- | Update the paddle position (keyboard event handling)
movePaddle :: Float      -- ^ New X pos
           -> GameStatus -- ^ Old state
           -> GameStatus -- ^ New state
movePaddle seconds game = newGame
  where
    newGame 
      | paddleWallCollision newX == N = game { playerLoc = newX }
      | otherwise                     = game
    -- | Old location and velocity of the paddle
    x = playerLoc game
    vx = playerVel game
    newX = x + vx * seconds
    hw = fromIntegral width / 2
    hpw = pWidth myPaddle / 2

-- | Handle mouse moving (paddle position change)
movePaddleMouse :: GameStatus -> Float -> GameStatus
movePaddleMouse game newX = game { playerLoc = newPos }
  where
    x = playerLoc game
    vx = playerVel game

    paddleWallCollisionType = paddleWallCollision newX
    -- Return the paddle to it possible position if it goes out
    newPos 
      | paddleWallCollisionType == R =  hw - hpw - borderW
      | paddleWallCollisionType == L = -hw + hpw + borderW
      | otherwise                    =  newX
      
    hpw = pWidth myPaddle / 2
    hw = fromIntegral width / 2

-- | Generate new line of blocks at the upper part of the field
generateRow :: GameStatus -> GameStatus
generateRow game = result 
  where
    result 
      | needAddMoreRow game = 
        game { blocks = newBlocks ++ shiftedOldBlocks, paddleBounceCounter = 0 }
      | otherwise = game
    newBlocks = foldl (\x (n, t) -> x ++ newBloc (n,t) ) 
      [] (zip [0..] (take 12 (randomRs (0,3) (mkStdGen (score game)))))
    
    -- | Generate a new block with given type t
    newBloc (n, t) 
      | t == 0 = []
      | otherwise = [Block
        { blockPos = coord2Pos (0, fromIntegral (mod (truncate n) 12))
        ,  blockRow = 0
        ,  blockColumn = column n
        ,  blockType = t
        }]

    column y = fromIntegral (mod (truncate y) 12)
    oldBlocks = blocks game
    shiftedOldBlocks = map (\x -> 
      x { blockRow = blockRow x + 1
        ,  blockPos = coord2Pos (blockRow x + 1, blockColumn x)  
        }
      ) oldBlocks
