module Constants where
import Graphics.Gloss

-- | Screen width.
width :: Int
width = 600

-- | Screen height.
height :: Int
height = 700

-- | Screen position.
offset :: Int
offset = 100

-- | Make game window.
window :: Display
window = InWindow "Arkanoid" (width, height) (offset, offset)

-- | Background color of screen.
background :: Color
background = white 

-- | Size of ball.
ballSize :: Float
ballSize = 5

addNewAfter :: Int
addNewAfter = 2

needAddMoreRow :: GameStatus -> Bool  
needAddMoreRow game = 
  mod (paddleBounceCounter game) (addNewAfter + 1) == addNewAfter


-- | Block information.
data Paddle = Paddle
  { pWidth :: Float 
  , pHeight :: Float
  , pColor :: Color
  , pLvl :: Float
  }

myPaddle :: Paddle
myPaddle = Paddle
  { pWidth = 150
  , pHeight = 10  
  , pLvl = -300
  , pColor = light blue
  }
-- | Border width (thickness)
borderW :: Float
borderW = 2.5

-- | Ball color.
ballColor :: Color
ballColor = red

-- | Number of frames to show per second.
fps :: Int
fps = 200

-- | Ball position alias.
type Position = (Float, Float)

-- | Block information.
data BlockInfo = Block
  { blockPos :: Position -- ^ (x, y) block location.
  , blockType :: Int
  , blockRow :: Int 
  , blockColumn :: Int
  } 


-- | Size of blocks.
blockSize :: (Float, Float)
blockSize = (30, 15)

blockColor :: Int -> Color
blockColor blockType = case blockType of 
  1 -> green 
  2 -> yellow
  _ -> magenta

-- | List of blocks.
type Blocks = [BlockInfo]

-- | Game status.
data GameStatus = Game
  { ballLoc :: Position -- ^ (x, y) ball location
  , ballVel :: Position -- ^ (x, y) ball velocity
  , playerLoc :: Float  -- ^ Player x position
  , playerVel :: Float  -- ^ Player x velocity
  , playerAcc :: Float  -- ^ Player acceleration
  , blocksH :: Float    -- ^ Lowest lvl for blocks
  , score :: Int        -- ^ Current score
  , paddleBounceCounter :: Int -- ^ Successive bounces counter
  , isPaused :: Bool    -- ^ Pause indicator
  , blocks :: Blocks    -- ^ Blocks currently on screen
  , gameStat :: Int     -- ^ Game status indicator. 0 - still playing, -1 - lost.
  }

-- | Starting state of the game.
initialState :: GameStatus
initialState = Game
  { ballLoc = (0, -120)
  , ballVel = (0, -170)
  , playerLoc = -0
  , playerVel = 0
  , playerAcc = 300
  , blocksH = -240
  , score = 0
  , paddleBounceCounter = 0
  , isPaused = True
  , blocks = foldl (\x y -> x ++ [Block
    { blockPos = coord2Pos (
          fromIntegral (truncate (y / 12)),
          fromIntegral (mod (truncate y) 12)
      ),
      blockRow = fromIntegral (truncate (y / 12)),
      blockColumn = fromIntegral (mod (truncate y) 12),
      blockType = if       mod (truncate y) 2 == 0 then 1
                  else if  mod (truncate y) 3 == 0 then 2
                       else 3
    }]) [] [0..47]
  , gameStat = 0
  }


-- | Placing blocks based on coords
coord2Pos :: (Integral a, Integral b) => (a, b) -> (Float, Float)
coord2Pos (r,c) = (x,y)
  where   
    x = right - (fromIntegral c * (bw + 15) ) - 20
    y = top - (fromIntegral r * (bh + 15) ) - 30
    top = fromIntegral height/2 - borderW - 30
    right = fromIntegral width/2 - borderW - 30
    (bw,bh) = blockSize