-- | Arkanoid game implemented in Haskell.
module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Constants
import Physics


-- | Convert state into a picture.
render :: GameStatus -- State that is being rendered.
       -> Picture    -- Picture that represents game state.
render game
  | gameStatus == 0
    = if isPaused game then
          pictures [ball, field, mkPlayer, drawBlocks, pauseMsg, cscore]
      else
          pictures [ball, field, mkPlayer, drawBlocks, cscore]
  | gameStatus == - 1
    = pictures
        [translate (- 275) 0 (color red (text "You lost! ") ),
        cscore,
        translate
          (- 250) (- 150)
          (scale 0.3 1 (color blue (text "Press <n> for new game!")))]
  | otherwise = blank 
  where
      field = color black
        (rectangleWire (w - borderW) (h - borderW))
      ball
        = uncurry translate (ballLoc game)
            $ color ballColor $ circleSolid ballSize
      mkPlayer
        = translate (playerLoc game) (pLvl myPaddle)
            $ color playerColor $ rectangleSolid (pWidth myPaddle) (pHeight myPaddle)
      playerColor = pColor myPaddle
      drawBlocks = blocksRender (blocks game)
      cscore = translate (-w/2 + borderW + 5) (-h/2 + borderW + 5 ) $ scale 0.3 0.3 
        $ color blue $ text (show (score game))
      gameStatus = gameStat game
      pauseMsg
        = translate (- 220) (- 50)
            $ scale 0.3 1 $ color blue $ text "Press <space> to start!"
      w = fromIntegral width
      h = fromIntegral height

-- | Render whole blocks picture from blocks
blocksRender :: [BlockInfo] -> Picture
blocksRender bs = result 
  where 
    result = pictures
      $ foldl(\ x y -> x ++ [blck y])
          [] bs
    blck y = 
      uncurry translate (blockPos y) $
      color (blockColor (blockType y)) $
      uncurry rectangleSolid blockSize


-- | Respond to key events
handleKeys :: Event -- Event to handle
           -> GameStatus -- Initial game status
           -> GameStatus -- Game status after the event
-- For 'r' keypress, game is returned to it's initial state
handleKeys (EventKey (Char 'n') Down _ _) game = initialState
-- For mouse moving, change paddle position
handleKeys (EventMotion (x, y)) game = movePaddleMouse game x
-- For 'p' keypress, game is paused/unpaused
handleKeys (EventKey (SpecialKey KeySpace) Down _ _) game = game { isPaused = not $ isPaused game }
-- For '←' keypress, move paddle to left
handleKeys (EventKey (SpecialKey KeyLeft) Down _ _) game = game { playerVel = playerVel game - playerAcc game }
-- For '←' release, stop the paddle
handleKeys (EventKey (SpecialKey KeyLeft) Up _ _) game = game { playerVel = playerVel game + playerAcc game }
-- For '→' keypress, move paddle to left
handleKeys (EventKey (SpecialKey KeyRight) Down _ _) game = game { playerVel = playerVel game + playerAcc game }
-- For '→' release, stop the paddle
handleKeys (EventKey (SpecialKey KeyRight) Up _ _) game = game { playerVel = playerVel game - playerAcc game }
-- All other inputs are ignored
handleKeys _ game = game


-- | Checks if there is any destroyable block left
tooManyBlocks :: Blocks -- List of block on screen
              -> Bool   -- Indicator whether there are destroyable blocks left
tooManyBlocks bs = max > 18
  where
    max = foldl (\y x -> if y < blockRow x then blockRow x else y ) 0 bs 

-- | Update the game by moving the ball
update :: Float      --  Seconds since last update
       -> GameStatus -- Old game status
       -> GameStatus -- New game status
update seconds game
  | isPaused game = game
  | y < - hh = game { gameStat = - 1 }
  | tooManyBlocks $ blocks game = game { gameStat = -1 }
  | otherwise = generateRow . paddleBounce . wallBounce
      $ moveBall seconds
      $ blockCollision
      $ movePaddle seconds game
  where
    (_, y) = ballLoc game
    hh = fromIntegral height / 2 - borderW 

-- | Window creation, game loop starting
main :: IO ()
main = play window background fps initialState render handleKeys update
